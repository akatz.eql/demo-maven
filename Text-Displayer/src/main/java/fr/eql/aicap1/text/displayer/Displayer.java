package fr.eql.aicap1.text.displayer;

import fr.eql.aicap1.text.generator.Generator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JOptionPane;

public class Displayer {

    private static final Logger logger = LogManager.getLogger();

    public void displayHelloWorld() {
        Generator generator = new Generator();
        String text = generator.generateHelloWorld();
        logger.info("Affichage de 'Hello World !'");
        JOptionPane.showMessageDialog(null, text);
    }
}
