package fr.eql.aicap1.text.generator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Generator {

    private static final Logger logger = LogManager.getLogger();

    public String generateHelloWorld() {
        logger.info("Génération de 'Hello World !'");
        return "Hello World !";
    }
}
